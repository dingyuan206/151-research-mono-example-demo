# Monorepo Preject User Manual 

## Front-ends

* [React](portal-react/README.md)

## Back-ends

* [Flask](api-flask/README.md)

## User Guide

Step by step guide to setup a CI/CD for the toy project which held in Bitbucket.

## Step 1. Setup mirror repository in gitlab

Repository mirroring allows for mirroring of repositories to and from external sources. It can be
used to mirror branches, tags, and commits between repositories.

A repository mirror at GitLab will be updated automatically. You can also manually trigger an update
at most once every 5 minutes.

Refer to:
- [gitlab repository mirror](https://gitlab.com/help/user/project/repository/repository_mirroring.md)
- It is also possible to configure the mirrored repository to trigger pipeline when code updated in original repo
![Trigger pipeline for mirror update](images/config_mirror_repo.png)

## Step 2. Configure the gitlab yml file for pipeline

### Define the stages

According to the old pipeline and expected usage, we can define stages as below:

- build
- test
- release_note
- deploy_dev
- test_end_to_end_dev
- deploy_prod
- test_end_to_end_prod

### Define pipeline structure

Here are several consideration points:
- One of the major requirement is to trigger the build/test/deploy according to the sub-folder (i.e. module). 
- So in backend and frontend folders, we put the yml config file respectively.
- Since the deploy is common task, either the backend change, or the frontend change, or both changed,
the deploy is required only once. 
- So the deploy and related end-to-end test are defined in the root directory.

About how to config trigger by the folder change, we can check the "only changes" syntax.

Refer to:
- [gitlab Yaml reference](https://docs.gitlab.com/ee/ci/yaml/README.html)
- https://itnext.io/deploying-a-vue-adonisjs-monorepo-to-heroku-via-gitlab-ci-cd-a4d87b31c20

### Complete each stage content

Be familiar with the frontend and backend technology, and determine the build and unit-test commands.

(1) For the backend, flask framework is used, and in its README.md, there are steps to build & test.

It uses python 3.7, and requires `pytest` tool.
So we just search the docker image from docker hub with keyword: "python 3.7"
And inside the test "stage", set the imge to `python:3.7-alpine`. And in "script" section,
fill in necessary script to do the test.

```bash
'[backend] Testing':
stage: test
image: "python:3.7-alpine"
script:
- cd api-flask
- pip install pytest
- pytest
```

Refer to:
- https://hub.docker.com/

(2) For the frontend, it uses react, also in its README.md, it explains how to build & test.
To find the proper image which contains the build env, we can search "react" from the dockerhub.

```bash
stage: test
image: "bayesimpact/react-base:latest"
script:
- cd portal-react
- npm install
- npm test --watchAll=false
```

(3) Verify the function of building docker image inside the pipeline  

Get familiar with basic docker build knowledge, and make the backend API support building as a docker image.

Add the Dockerfile to backend folder, the content is like this:
```docker
FROM python:3.7-alpine
ADD . /src
WORKDIR /src
RUN pip install gunicorn
RUN pip install -r requirements.txt

EXPOSE 5000
CMD ["/bin/sh", "/src/entrypoint.sh"]
```

Search the gitlab document, and follow the step to integrate docker build.

```yml
stage: build
image: docker:19.03.0
services:
- docker:19.03.0-dind
variables:
DOCKER_DRIVER: overlay2
DOCKER_TLS_CERTDIR: ""
before_script:
- docker info
- apk add curl
script:
- echo "This is a backend build on api-flask, and it may be a static type checking only"
- cd api-flask
- docker build -t my-docker-image .
- docker run -d -p 5000:5000 my-docker-image
```

Refer to:
- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
- https://docs.docker.com/engine/reference/commandline/build/

(4) Verify access AWS from the pipeline

Detail Steps:

- Create the AWS account
- Create the IAM user
-  Config the aws access key in gitlab

![Config the gitlab for aws access key](images/config_aws.png)

- Create AWS instance
- Use awscli command in pipeline (first needs to install `awscli` tool

```yml
# define deploy tasks
'Updating Dev':
stage: deploy_dev
image: "python:3.7-alpine"
before_script:
- apk add openssh
- apk add zip
script:
- echo "Deploy to develop environment"
- pip install awscli
- aws ec2 describe-instances --region=ca-central-1
```


(5) Deploy to AWS env

Setup the AWS instance, and write the re-deploy script to support deploy application automatically.  
Here we also need to be familiar with the toy project, and know how to integrate it to one page.  

```bash
#!/bin/bash
# stop old app
pkill -f 'python3 app.py' > /dev/null 2>&1
pkill -f node  >/dev/null 2>&1

cd $HOME/deploy

# clean old env
rm -rf dev
unzip artifact.zip -d dev/
cd dev/api-flask && pip3 install -r requirements.txt
nohup python3 app.py &>/dev/null &
cd $HOME/deploy/dev/portal-react && npm install --ignore-scripts
nohup npm start  &>/dev/null &
echo "Redeploy success!"
```

And write the yml config as below:

```yml
'Updating Dev':
stage: deploy_dev
image: "python:3.7-alpine"
before_script:
- apk add openssh
- apk add zip
script:
- echo "Deploy to develop environment"
- pip install awscli
- aws ec2 describe-instances --region=ca-central-1
- echo "$EC2_PEM"
- chmod 400 "$EC2_PEM"
- zip -r artifact.zip .
- scp -o StrictHostKeyChecking=no -i "$EC2_PEM" artifact.zip ubuntu@15.223.77.125:deploy
- ssh -o StrictHostKeyChecking=no -i "$EC2_PEM" ubuntu@15.223.77.125 bash redeploy.sh
```


### Step 3: Release Notes Tool

When pipeline was created, there are two variables corresponding to the commit sya.
-  CI_COMMIT_BEFORE_SHA
-  CI_COMMIT_SHA

And we can extract the commit messages from the commit message according to the rule.
So it is easy to write the bash script to do this job.

```bash

if [[ $# -lt 3 ]]; then
echo "Usage: $0 release_type sha_before sha_current"
echo "  release_type: required. values are: web or api"
echo "  sha_before: required. The commit before this pipeline"
echo "  sha_current: required. The commit in this pipeline"
exit
fi

# Get release notes by given a commit count relative to current HEAD commit
function getReleaseMessage() {
str_type="$1"
from_sya="$2"
to_sya="$3"
if [ "$str_type" == "web" ]; then
# Note 1): we can revert the commit list by tool: tac
# Note 2): we can remove the ticket flag by tool: cut -c16-
notes=$(git log "$from_sya"..."$to_sya" --pretty=%B | grep -P "#ticket: (GV-02|GV-01|GV-04)")
elif [ "$str_type" == "api" ]; then
# Note 1): we can revert the commit list by tool: tac
# Note 2): we can remove the ticket flag by tool: cut -c16-
notes=$(git log "$from_sya"..."$to_sya" --pretty=%B | grep -P "#ticket: (GV-03|GV-01|GV-04)")
else
notes="Invalid release type specified: $str_type."
fi
echo "$notes"
}

getReleaseMessage $1 $2 $3
# getReleaseMessage 5 "api"
```

