import React, { Component } from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
            Front-End: HI FLASK!
      </header>
      <BackendMessage />
    </div>
  );
}

class BackendMessage extends Component {
  constructor() {
      super();
      this.state = {
          msg: null,
      }
  }
  componentDidMount() {
      var protocol = window.location.protocol;
      var slashes = protocol.concat("//");
      var host = slashes.concat(window.location.hostname);
      var apiEndPoint = host + "/api";
      fetch(apiEndPoint)
      .then(res => {
          console.log('res: ', res);
          return res.json();
      }).then(data => {
          console.log('data:', data);
          this.setState(data);
          console.log("state:", this.state.msg);
      })
  }
  render() {
      return (
          <div class="backend">API: {this.state.msg}</div>
      );
  }
}

export default App;

