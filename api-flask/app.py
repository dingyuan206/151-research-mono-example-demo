from flask import Flask, jsonify
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

from controllers import hello_world

@app.route('/', methods=['GET'])
@cross_origin()
def index():
    return jsonify(hello_world())

if __name__ == '__main__':
 app.run(port=8090)

