# API - Flask

## Prerequisites

* Python3.7
* Virtualenv

## Running

* Create virtual env:
`virtualenv -p python3.7 venv`
* Enter venv: `source venv/bin/activate`
* Install requirements: `pip install -r requirements.txt`
* Start server: `python app.py`
* Test: `curl http://localhost:5000`

## Testing

* Enter venv: `source venv/bin/activate`
* Install pytest: `pip install pytest`
* Run tests: `pytest`

## changes

1. test

