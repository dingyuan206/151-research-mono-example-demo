#!/bin/bash

if [[ $# -lt 3 ]]; then
  echo "Usage: $0 release_type sha_before sha_current"
  echo "  release_type: required. values are: web or api"
  echo "  sha_before: required. The commit before this pipeline"
  echo "  sha_current: required. The commit in this pipeline"
  exit
fi

# Get release notes by given a commit count relative to current HEAD commit
function getReleaseMessage() {
  str_type="$1"
  from_sya="$2"
  to_sya="$3"
  if [ "$str_type" == "web" ]; then
    # Note 1): we can revert the commit list by tool: tac
    # Note 2): we can remove the ticket flag by tool: cut -c16-
    notes=$(git log "$from_sya"..."$to_sya" --pretty=%B | grep -P "#ticket: (GV-02|GV-01|GV-04)")
  elif [ "$str_type" == "api" ]; then
    # Note 1): we can revert the commit list by tool: tac
    # Note 2): we can remove the ticket flag by tool: cut -c16-
    notes=$(git log "$from_sya"..."$to_sya" --pretty=%B | grep -P "#ticket: (GV-03|GV-01|GV-04)")
  else
    notes="Invalid release type specified: $str_type."
  fi
  echo "$notes"
}

getReleaseMessage $1 $2 $3
# getReleaseMessage 5 "api"

