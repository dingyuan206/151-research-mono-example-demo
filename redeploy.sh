#!/bin/bash
# stop old app
pkill -f 'python3 app.py' > /dev/null 2>&1
pkill -f node  >/dev/null 2>&1

cd $HOME/deploy

# clean old env
rm -rf dev
unzip artifact.zip -d dev/
cd dev/api-flask && pip3 install -r requirements.txt
nohup python3 app.py &>/dev/null &
cd $HOME/deploy/dev/portal-react && npm install --ignore-scripts
nohup npm start  &>/dev/null &
echo "Redeploy success!"
